---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-11-30

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 397   | 100%        |
| Based in the US                           | 224   | 56.42%      |
| Based in the UK                           | 25    | 6.30%       |
| Based in the Netherlands                  | 15    | 3.78%       |
| Based in Other Countries                  | 133   | 33.50%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 397   | 100%        |
| Men                                       | 307   | 89.50%      |
| Women                                     | 90    | 26.24%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 34    | 100%        |
| Men in Leadership                         | 28    | 82.35%      |
| Women in Leadership                       | 6     | 17.65%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 191   | 100%        |
| Men in Development                        | 162   | 89%         |
| Women in Development                      | 29    | 11.03%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        |224    | 100%        |
| Asian                                     | 18    | 8.04%       |
| Black or African American                 | 4     | 1.79%       |
| Hispanic or Latino                        | 14    | 6.25%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.45%       |
| Two or More Races                         | 9     | 4.02%       |
| White                                     | 119   | 53.13%      |
| Unreported                                | 59    | 26.34%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 66    | 100%        |
| Asian                                     | 7     | 10.61%      |
| Black or African American                 | 2     | 3.03%       |
| Hispanic or Latino                        | 6     | 9.09%       |
  Two or More Races                         | 3     | 4.55%       |
| White                                     | 34    | 51.52%      |
| Unreported                                | 14    | 21.21%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 27    | 100%        |
| Asian                                     | 4     | 14.81%      |
| Native Hawaiian or Other Pacific Islander | 1     | 3.70%       |
| White                                     | 12    | 44.44%      |
| Unreported                                | 10    | 37.04%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 397   | 100%        |
| Asian                                     | 35    | 8.82%       |
| Black or African American                 | 9     | 2.27%       |
| Hispanic or Latino                        | 21    | 5.29%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.25%       |
| Two or More Races                         | 12    | 3.02%       |
| White                                     | 203   | 51.13%      |
| Unreported                                | 116   | 29.22%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 191   | 100%        |
| Asian                                     | 20    | 10.47%      |
| Black or African American                 | 6     | 3.14%       |
| Hispanic or Latino                        | 13    | 6.81%       |
| Two or More Races                         | 6     | 3.14%       |
| White                                     | 93    | 48.69%      |
| Unreported                                | 53    | 27.75%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 34    | 100%        |
| Asian                                     | 4     | 11.76%      |
| Hispanic or Latino                        | 1     | 2.94%       |
  Native Hawaiian or Other Pacific Islander | 1     | 2.94%       |
| White                                     | 14    | 41.18%      |
| Unreported                                | 14    | 41.18%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 397   | 100%        |
| 18-24                                     | 18    | 4.53%       |
| 25-29                                     | 92    | 23.17%      |
| 30-34                                     | 111   | 27.96%      |
| 35-39                                     | 65    | 16.37%      |
| 40-49                                     | 73    | 18.39%      |
| 50-59                                     | 32    | 8.06%       |
| 60+                                       | 4     | 1.01%       |
| Unreported                                | 2     | 0.50%       |
