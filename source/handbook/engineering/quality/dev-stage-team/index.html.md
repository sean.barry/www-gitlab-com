---
layout: markdown_page
title: "Dev stage team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Engineers in this team are embedded in the **Dev** cross-functional group and
are responsible for coverage of **Dev** stage
[product category](/product/categories/).

## Areas of Responsibility

* **[Manage]**: Configuration, Admin, Groups & Subgroups, i18n
  * Third-party Authentication: Oauth, LDAP, SAML
* **[Plan]**: Project Management, Issues, Boards, Epics, Roadmap, Markdown, Labels, Search, Todos, Weights, Due dates
  * Third-party Chat Integrations: Slack, Mattermost
  * Third-party Project Management: Jira, etc.
  * Third-party Communication: Email
* **[Create]**: Code Review, Merge Requests, Diffs, Approvals, Web IDE, Wikis, Snippets
  * Source Control: Git Operations, Gitaly
  * Third-party Source Control: GitHub, Bitbucket

## Members

* [Dan Davison](/company/team/#sircapsalot): Test Automation Infrastructure, Maintainer of the [Selenium Docker project](https://github.com/SeleniumHQ/docker-selenium).
* [Mark Lapierre](/company/team/#mdlap): Test Automation Strategy for **Create**, Cognitive Psychology (Ph.D.)
* [Ramya Authappan](/company/team/#atramya): Test Automation Strategy for **Plan**
* [Sanad Liaquat](/company/team/#sanadliaquat): Test Automation Strategy for **Manage**

[Manage]: /handbook/product/categories/#Manage
[Plan]: /handbook/product/categories/#Plan
[Create]: /handbook/product/categories/#Create
