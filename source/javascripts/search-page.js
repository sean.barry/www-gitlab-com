(function() {
  var animatedIcons = document.getElementsByClassName('animation-icon');

  for (var i = 0; i < animatedIcons.length; i++) {
    var randomDelay = Math.random().toFixed(1) * 10;
    animatedIcons[i].style.animationDelay = randomDelay + 's';
  }
})();
