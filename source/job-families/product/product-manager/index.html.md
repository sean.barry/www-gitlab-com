---
layout: job_family_page
title: "Product Manager"
---

## Role

Product Managers at GitLab have a unique opportunity to define the future of the
entire [DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). We
are working on a [single application](/handbook/product/single-application) that
allows developers to invent, create, and deploy modern applications.

We want to facilitate onboarding to [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/):
users may not be aware of its potential, we will show them how easy and powerful
it is! We want to support large teams working on mission-critical software
across the entire company, and also single developers starting a new visionary
project from scratch.

We work in a very unique way at GitLab, where flexibility and independence meet
a high paced, pragmatic way of working. And everything we do is [in the open](https://about.gitlab.com/handbook).

We are looking for talented product managers that are excited by the idea to
contribute to our vision. We know there are a million things we can and want to
implement in GitLab. Be the one making decisions.

We encourage people to apply even if they don't have an established background
in this role yet, but they feel this page describes exactly what they have a
strong attitude for. Product managers always start off somewhere, and it's ok if
that somewhere is GitLab.

We recommend looking at our [about page](/about) and at the [product handbook](https://about.gitlab.com/handbook/product/)
to get started.

<a id="base-pm-requirements"></a>
## Responsibilities

- **Drive the product in the right direction**
  - Build an effective [roadmap](/handbook/product/#3-month-roadmap) to
    prioritize important features properly
  - Take high-level feature proposals and customer problems and break them into [small iterations](/handbook/values/#iteration)
    that engineering can work on
  - [Balance](/handbook/product/#prioritization) new features, improvements, and
    bugfixes to ensure a high velocity and a stable product
  - Consider the business impact, [ROI](https://en.wikipedia.org/wiki/Return_on_investment),
    and other implications when taking important decisions

- **Take an active role in defining the future**
  - Contribute to the [product vision](/direction/#vision), together with the
    Head of Product and VP of Product
  - Create and maintain a [vision for your product area](/handbook/product/#stage-vision)
  - Create and maintain the [vision for each product category](/handbook/product/#category-vision)
  - Innovate within your product area by proposing [ambitious](/handbook/product/#be-ambitious)
    features
  - Follow innovation in your product area
  - Communicate and evangelize your product vision internally and among the
    wider community

- **Manage the product lifecycle end-to-end**
  - Follow feature development end-to-end; provide guidance and feedback to
    engineers and designers; ensure everyone is aligned
  - Be the voice of the customer and the [subject-matter expert](https://en.wikipedia.org/wiki/Subject-matter_expert)
    for your group
  - Contribute to documentation, blog posts, demos, and marketing materials for
    product features
  - Collaborate with other Product Managers, UX, and engineers in cross-area
    features to build a [single application](/handbook/product/single-application)
  - Manage the
    [uncertainty](https://www.cleverpm.com/2018/08/23/accepting-uncertainty-is-the-key-to-agility/)
    in an efficient way, adjusting plans to new working conditions
  - Drive teams to acquire necessary data in order to back assumptions, show value, and drive prioritization

- **Engage with stakeholders in two-way communication**
  - Assist Sales, Support, Customer Success, and Marketing as the [subject-matter expert](https://en.wikipedia.org/wiki/Subject-matter_expert)
    for your area
  - [Talk to customers](/handbook/product/#customer-meetings) and engage with
    the community regularly
  - Engage with analysts on briefings and product evaluations
  - Work with the entire Product team to share improvements and best practices

## You are _not_ responsible for

- **A team of engineers:** you will take the lead in decisions about the
  product, but not manage the people implementing it
- **Capacity planning:** you will define priorities, but the Engineering Manager
  evaluates the amount of work possible
- **Shipping in time:** you will work in a group, but the group is responsible
  for shipping in time, not you

## Requirements

- Experience in product management
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space
- Strong technically: you understand how software is built, packaged, and deployed
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values), and work in accordance with those values
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

***

## Levels

Read more about [levels](/handbook/hiring/#definitions) at GitLab here.

### Product Manager

Product Manager requirements and responsibilities are outlined [above](#base-pm-requirements).

### Senior Product Manager

The Senior Product Manager role extends the [Product Manager](#base-pm-requirements) role. Senior Product Managers are expected to be experts in their product domain and viewed as such to the community and internally at GitLab. They are expected to prioritize and manage their products with minimal guidance from leadership or other product team members.

#### Responsibilities

* Manage features and solutions that span multiple stages, and/or benefit other stages
* Make sound decisions for your product/stage, building trust, and driving clarity across all teams at GitLab and the community
* Mastery of the competitive and market landscape of their product domain and understanding how this landscape impacts their own product roadmap
* Have a strong presence in customer advisory boards and other customer feedback forums to fully understand customer pain points and how these can be solved in the future
* Mentors junior or less experienced Product Managers to quickly bring them up to speed and add value
* Ability to clearly document ROI or impact for a given action, feature, or prioritization action
* Represents GitLab as a product and domain expert in front of Industry Analysts, customer visits, industry events/conferences, and other events.
* Ability to present to C-level executives both internally at GitLab and externally to customers and prospects
* Conduct advanced [ROI](https://en.wikipedia.org/wiki/Return_on_investment) analysis on investment cases to determine if GitLab should pursue them 

#### Requirements

* 5+ years product management experience
* Excellent written and oral communication skills
* Ability to think like a CEO to drive success of your product area
* Fantastic problem solving skills and attention to detail

***

## Specialties

### Verify (CI)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous integration (CI), code quality analysis, micro-service testing, usability testing, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Significant experience with Kubernetes and Docker

### Release (CD)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous delivery (CD), release orchestration, features flags, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker

### Configure

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out configuration management and other operations-related features such as ChatOps.

#### Requirements

- Strong understanding of CI/CD, configuration management, and operations
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker

### Serverless

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out serverless application and function management.

#### Requirements

- Strong understanding of DevOps and cloud-native application development
- Understanding of deployment infrastructure, container technologies, and serverless functions
- Significant experience with Kubernetes and Docker

### Packaging

We're looking for product managers to cover the [Package stage](/handbook/product/categories/#package) of our DevOps lifycle. This candidate will work specifically on building out packaging categories and features such as Docker container registry and binary artifact management.

#### Requirements

- Strong understanding of CI/CD and package management
- Understanding of the complexity of managing multi-artifact application deployment
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker

### Distribution

We're looking for product managers to support our [Distribution group](/handbook/product/categories/#admin) and manage our installation, upgrade, and configuration process for our self-managed customers. 

#### Requirements

- Strong understanding of system administration
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes

### Secure

We're looking for product managers that can help us work on the future of developer tools; specifically, building out application security testing, including static analysis and dynamic testing.

#### Requirements

- Strong understanding of CI/CD and automated security testing
- Understanding of deployment infrastructure and container technologies such as Kubernetes and Docker

### Growth

Get more people to use and contribute to GitLab, mainly the Community Edition (CE). Marketing gets us awareness and downloads. Your goal is to ensure people downloading our product keep using it, invite others, and contribute back. You report to the VP of Product.

#### Goals

1. Conversion of download to 30 day active (install success * retention)
1. Growth of existing CE installations
(growth of comments measured with usage ping)
1. Growth in the number of new people that contributed merged code

#### Possible Improvements

1. Auto install let's encrypt
1. Check open to new users by default
1. Check invite by email function
1. Usage ping for CE
1. Ensure installers are very simple
1. First 60 seconds with a product
1. Measure contributors like a SaaS funnel (activation, retention)
1. Improve data for Usage ping EE
1. Improve zero-states of project (hints on first action)
1. New feature show-off / highlighting
1. Great experience when not logged in (commenting)
1. Great experience when not a project member (edit button works)
1. Make it easier to configure email
1. Make it easier to configure LDAP

#### You'll work with

* Other PM's
* Developers (implement some things self, some with help of experts)
* Community team
* Developers
* Merge request coaches

#### Requirements

- Deep understanding of UX and UI
- You've built web applications before in Ruby on Rails (be it personal or professional)
- You're pragmatic and willing to code yourself
- You're able to independently find, report and solve issues and opportunities related to growth in the product
- Good understanding of Git
- Able to make wireframes and write clear, concrete product specifications

#### Responsibilities

- Plan and execute on improvements in GitLab related to growth
- Write specs and create wireframes to communicate your plans
- Ship improvements every month and make it possible to report on those
improvements
- Do data analysis whenever useful
- Assist the rest of the team with topics related to growth
- Build and expand tools related to growth (version.gitlab.com and others)

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

### Meltano (BizOps Product)

We're looking a product manager to shape the future of data science tools. You would be taking GitLab's approach to complete devops, and apply it to Meltano. Meltano is an open source convention-over-configuration framework for data science, analytics, business intelligence, and data science. We will leverage version control, CI, CD, Kubernetes, and review apps. We aim to help customers answer difficult questions e.g., how can I acquire the highest customer lifetime value (LTV) at the lowest customer acquisition cost (CAC). We are creating open source, deeply-integrated tools without using traditional BI tools.

It'll be your job to work out what we are going to do and how. We need a full-stack PM, i.e. someone who can write code, update pipelines, and do whatever it takes to get things done and shape the future of data science.

#### Requirements

- Strong understanding of business operations, business intelligence, data warehousing, etc.
- Experience with CI/CD and containers. Knowledge of Kubernetes and GitLab CI/CD a plus.
