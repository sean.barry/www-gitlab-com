---
layout: markdown_page
title: "Using Gitlab with an agile development methodology"
description: "How to use GitLab as an agile project management tool for agile processes such as scrum, kanban, and scaled agile framework."

---
## Agile project management

| Since the publication of the [2001 Agile Manifesto](http://agilemanifesto.org/), development teams have created iterative, incremental, and lean approaches to streamline and accelerate the delivery of software projects. The techniques have ranged from 'extreme programming' to [Scrum](https://www.scrum.org/), and [Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) where teams are able to organize, plan, and deliver working software.  Large enterprises have adopted agile at enterprise scale in many frameworks, ranging from "[Scaled Agile Framework (SAFe)](https://www.scaledagile.com/)" to [Disciplined Agile Delivery](http://www.disciplinedagiledelivery.com/) GitLab enables teams to apply agile practices and principles to organize and manage their work. Because agile is cool.  | ![Agile Manifesto](https://www.keystepstosuccess.com/wp-content/uploads/2016/08/agile_manifesto_original.jpg)  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

GitLab is designed to be a **flexible and agnostic agile project management tool** to empower you to tailor it to meet **your agile processes and methodology** (not the other way around).   Below are several examples of how GitLab can support specific agile development methodologies.


## Scrum

| [Scrum](https://www.scrum.org/resources/what-is-scrum) is an agile development framework where teams establish a consistent cadence to organize and deliver value. A scrum team is cross-functional development team, with three defined 'roles' Product Owner, Scrum Master and Team member. The the Product Owner represents the business and users. The 'Scrum Master' protects the team from external distractions and helps them organize and plan their work. The Scrum team manages their work and breaks down the  work into short increments of Sprints ranging from a week to several weeks.  During the sprint, the team focuses on developing and delivering tangible value, typically in the form of working software, that meets the needs of the Product Owner.  |   ![Scrum](https://upload.wikimedia.org/wikipedia/commons/d/df/Scrum_Framework.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

#### Sprint
A sprint represents a finite time period in which the work is to be completed, often 1-3 weeks in duration. The Product Owner and the agile development team collaborate to decide what work that is in scope for the upcoming sprint.


| ![Sprint burndown chart](https://docs.gitlab.com/ee/user/project/milestones/img/burndown_chart.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ | GitLab's milestones and burndown charts enable teams to establish time-boxed work intervals and then focus on delivery and velocity. The team simply assigns milestones a start date and a due date to capture the time period of the sprint. The team then pulls issues into that sprint by assigning them to that particular milestone. |


####  Sprint planning / user stories
User stories describe the scope, goals and objectives of new functionality.  User stories also drive an estimation of the technical effort to implement the story.

| In GitLab, [issues](https://docs.gitlab.com/ee/user/project/issues/index.html) are where user stories are captured. GitLab Issues have a **weight** attribute to indicate the estimated effort (think of this as 'story points').  Typically, user stories are further broken down to technical deliverables, sometimes documenting technical plans or architecture. In GitLab, this information can be documented in the issue via task lists or in the [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html). | ![User Story / Issue](https://docs.gitlab.com/ee/user/project/issues/img/issues_main_view.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |


#### Daily scrum / scrum board
Throughout the sprint, issues move through various stages, such as *Backlog*, *In progress*, *Completed*, and *Accepted* depending on the workflow in your particular organization. Typically these are columns in an Agile board.

| ![Scrum Board](https://docs.gitlab.com/ee/user/project/img/group_issue_board.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ | In GitLab, [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html) allow you to define your stages, prioritize work in each stage and move issues across the board via drag and drop. The team configures the board with respect to the milestone and other relevant attributes. During daily stand-ups, the team reviews the board together to view the status of the sprint from a workflow perspective. |

#### Sprint review
The agile development team stays on track in real time and mitigates risks as they arise.  At the end of the sprint, the development team demos completed features to various stakeholders.

![GitLab Review App](https://docs.gitlab.com/ee/ci/review_apps/img/review_apps_preview_in_mr.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 }

With GitLab, this process is made simple using [GitLab Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) so that changes to the code can be demoed prior to being pushed to staging or production environments. Review Apps and [CI/CD pipelines](https://docs.gitlab.com/ee/ci/README.html) are integrated with the [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/index.html) itself, creating a collaborative hub for all the development work.  In fact, the merge request is home for [code reviews](https://docs.gitlab.com/ee/user/discussions/index.html#merge-request-reviews-premium) and approvals. This makes the merge request a powerful tool for Developers, QA and all stakeholders to track progress and  software quality, whether through automated testing with CI/CD, or manual testing in a Review App environment.

#### Sprint retrospective
Sprint Retrospectives look back at the previous sprint asking questions such as "what went right," " what could have gone better," and "what will we do different next time?"

In GitLab, teams use milestones, issues and issue boards to review the progress from their previous sprint. Their discussions and lessons learned can be easily be recorded and maintained in [GitLab Wiki](https://docs.gitlab.com/ee/user/project/wiki/index.html) pages for future reference. | ![GitLab Wiki](https://docs.gitlab.com/ee/user/project/wiki/img/wiki_create_home_page.png)‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |



#### Product backlog
The product or business owners typically create these user stories to reflect the needs of the business and customers. They are prioritized in a product backlog to capture urgency and desired order of development. The product owner communicates with stakeholders to determine the priorities and constantly refines the backlog.

| In GitLab, dynamically generated issue lists can be viewed to track backlogs. Labels are created and assigned to individual issues, which then enables filtering of issue lists by a single label or multiple labels. Labels can also be prioritized to assist in ordering the issues in those lists. | ![Issue Backlog List](https://docs.gitlab.com/ee/user/project/issues/img/project_issues_list_view.png)‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |



<center><a href="/sales" class="btn cta-btn orange">Contact sales and learn more about GitLab and scrum</a></center>

## Kanban

| [Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) is an agile software delivery approach based on the lean manufacturing principles of visualizing work in order to manage the 'work in process' (WIP) and reduce non-value activities and waste. The goal of Kanban is to focus on **flow** of value and allow a team to quickly re-prioritize work based on customer demand. Where Scrum is divided into fixed time 'Sprints', Kanban is focused on the capacity of the team to continuously deliver a flow of new features (value). | ![Kanban Board](https://upload.wikimedia.org/wikipedia/commons/f/f5/Kanban_board_example.jpg) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾   |

#### Kanban board
The Kanban board is an agile development model where the focus is on managing the continuous flow of new features and changes based on the capacity of the team.  Based on visual management techniques from Lean Manufacturing, the board is organized into columns representing the backlog, current 'work in process' and work that is 'done', where individual 'cards' move from stage to stage.  The Kanban board is a key tool to visualize the work so that teams can manage and minimize "work in process" (WIP).

| In GitLab, issue boards can easily be configured to support Kanban delivery practices.  Simply create a board, and add the columns for your process (defined by labels).  The key is to pay close attention to the capacity of the team in each stage of the work and to leverage the [issue weights](https://docs.gitlab.com/ee/user/project/issue_board.html#sum-of-issue-weights) of all the issues in process at any point in time. | ![GitLab Kanban Board](https://about.gitlab.com/images/gitlab-kanban-board.png) ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ |

#### Issue weights
The total amount of 'work' in any stage of the Kanban Board is summarized at the top of the board.  This enables you to plan and manage, given the capacity of your team.

![Issue Weight](https://docs.gitlab.com/ee/user/project/img/issue_board_summed_weights.png)

#### Backlog
The backlog is managed as a list of issues related to the project with a specific label. Typically the backlog is a list in the Kanban board, but can also be a list of open issues.

| ![Issue Backlog List](https://docs.gitlab.com/ee/user/project/issues/img/project_issues_list_view.png)‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾ | In Gitlab, you can manage your backlog either in the Kanban board or as a list of issues, whichever works best for you.  It's easy to add labels to your issues and then prioritize, filter and organize your backlog to best meet the business demand.  |

<center><a href="/sales" class="btn cta-btn orange">Contact sales and learn more about GitLab and kanban</a></center>

## SAFe
[SAFe](https://www.scaledagileframework.com/about/) is a Scaled Agile Framework provided by Scaled Agile Inc. It is an enterprise-level framework in which work moves through the portfolio, program, and team in an orderly, hierarchical way, providing suggested metrics and best practices in each area.
[Learn more about how GitLab supports the Scaled Agile Framework](/solutions/agile-delivery/scaled-agile/)

## Resources
* [GitLab Project Documentation](https://docs.gitlab.com/ee/user/project/)
* [GitLab for Agile](https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/)
* [4 ways to use issue boards](https://about.gitlab.com/2018/08/02/4-ways-to-use-gitlab-issue-boards/)
* [GitLab project mgt youtube](https://www.youtube.com/watch%3Fv%3DLB5zvfjIDi0&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwA3oECAUQAQ&usg=AOvVaw1yAMfhvN3pjNciSoNUmDjQ)
* [GitLab, Jira and Jenkins youtube](https://www.youtube.com/watch%3Fv%3DJn-_fyra7xQ&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwE3oECAEQAQ&usg=AOvVaw3jCWmPspuIWQqz0YfMwRG1)
* [GitLab and atlassian](https://www.youtube.com/watch%3Fv%3Do7pnh9tY5LY&ved=2ahUKEwjgrJ-I6OrdAhWpc98KHSRMCSwQjjgwBHoECAgQAQ&usg=AOvVaw0fHEQPLR8-oJnaweIwXQdo)
